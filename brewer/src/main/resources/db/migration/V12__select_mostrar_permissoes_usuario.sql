SELECT 	*FROM usuario

SELECT *FROM usuario_grupo

INSERT INTO usuario_grupo (codigo_usuario, codigo_grupo) VALUES (
	(SELECT codigo FROM usuario WHERE email = 'admin@admin.com'), 1);

SELECT u.email usuario
	, group_concat(SUBSTRING(p.nome, 6) ORDER BY p.nome separator ',') permissao
FROM usuario u 
	, usuario_grupo ug
	, grupo g 
	, grupo_permissao gp 
	, permissao p 
WHERE ug.codigo_usuario = u.codigo
	AND ug.codigo_grupo = g.codigo
	AND g.codigo = gp.codigo_grupo
	AND gp.codigo_permissao = p.codigo
GROUP BY usuario